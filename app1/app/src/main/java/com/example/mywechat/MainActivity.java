package com.example.mywechat;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Fragment mTab01 = new weixinFragment();
    private Fragment mTab02 = new frdFragment();
    private Fragment mTab03 = new contactFragment();
    private Fragment mTab04 = new settingFragment();

    //private LinearLayout mTabMessage;
    //private LinearLayout mTabFriend;
    //private LinearLayout mTabAddress;
    //private LinearLayout mTabSetting;

    private ImageButton mImgMessage;
    private ImageButton mImgFriend;
    private ImageButton mImgAddress;
    private ImageButton mImgSetting;



    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        initView();
        initFragment();
        initEvent();
        setSelect(0);
    }

    private void initFragment(){
        fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.id_content,mTab01);
        transaction.add(R.id.id_content,mTab02);
        transaction.add(R.id.id_content,mTab03);
        transaction.add(R.id.id_content,mTab04);
        transaction.commit();
    }

    private void initView() {
        //mTabMessage = findViewById(R.id.id_tab_weixin);
        //mTabFriend = findViewById(R.id.id_tab_frd);
        //mTabAddress = findViewById(R.id.id_tab_contact);
        //mTabSetting = findViewById(R.id.id_tab_setting);

        mImgMessage = findViewById(R.id.id_tab_weixin_img);
        mImgFriend = findViewById(R.id.id_tab_frd_img);
        mImgAddress = findViewById(R.id.id_tab_contact_img);
        mImgSetting = findViewById(R.id.id_tab_setting_img);
    }

    private void initEvent(){
        mImgMessage.setOnClickListener(this);
        mImgFriend.setOnClickListener(this);
        mImgAddress.setOnClickListener(this);
        mImgSetting.setOnClickListener(this);
    }

    private void setSelect(int i){
        FragmentTransaction transaction = fm.beginTransaction();
        hideFragment(transaction);
        switch (i) {
            case 0:
                transaction.show(mTab01);
                mImgMessage.setImageResource(R.drawable.weixin_pressed);
                break;

            case 1:
                transaction.show(mTab02);
                mImgFriend.setImageResource(R.drawable.lianxiren_pressed);
                break;

            case 2:
                transaction.show(mTab03);
                mImgAddress.setImageResource(R.drawable.faxian_pressed);
                break;

            case 3:
                transaction.show(mTab04);
                mImgSetting.setImageResource(R.drawable.shezhi_pressed);
                break;
            default:
                break;
        }
        transaction.commit();

    }

    private void hideFragment(FragmentTransaction transaction) {
        transaction.hide(mTab01);
        transaction.hide(mTab02);
        transaction.hide(mTab03);
        transaction.hide(mTab04);
    }

    public void onClick(View v){
        resetImg();
        switch (v.getId()){
            case R.id.id_tab_weixin_img:
                setSelect(0);
                break;
            case R.id.id_tab_frd_img:
                setSelect(1);
                break;
            case R.id.id_tab_contact_img:
                setSelect(2);
                break;
            case R.id.id_tab_setting_img:
                setSelect(3);
                break;
            default:
                break;
        }
    }

    public void resetImg(){
        mImgMessage.setImageResource(R.drawable.weixin);
        mImgFriend.setImageResource(R.drawable.lianxiren);
        mImgAddress.setImageResource(R.drawable.faxian);
        mImgSetting.setImageResource(R.drawable.shezhi);
    }
}